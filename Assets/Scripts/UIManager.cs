﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    /// <summary>
    /// A reference to all the action buttons
    /// </summary>
    [SerializeField]
    private Button[] actionButtons;

    /// <summary>
    /// Keycodes used for executing the action buttons
    /// </summary>
    private KeyCode action1, action2, action3;

	// Use this for initialization
	void Start ()
    {
        //Keybinds
        action1 = KeyCode.Alpha1;
        action2 = KeyCode.Alpha2;
        action3 = KeyCode.Alpha3;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(action1))//If we click button 1,2 or 3
        {
            ActionButtonOnClick(0);
        }
        if (Input.GetKeyDown(action2))
        {
            ActionButtonOnClick(1);
        }
        if (Input.GetKeyDown(action3))
        {
            ActionButtonOnClick(2);
        }
    }

    /// <summary>
    /// Executes an action based on the button clicked
    /// </summary>
    /// <param name="btnIndex"></param>
    private void ActionButtonOnClick(int btnIndex)
    {
        actionButtons[btnIndex].onClick.Invoke();
    }

}
