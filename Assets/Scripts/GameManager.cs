﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {
    
    /// <summary>
    /// A reference to the player object
    /// </summary>
    [SerializeField]
    private Player player;
	
	// Update is called once per frame
	void Update ()
    {
        //Executes click target
        ClickTarget();
	}

    private void ClickTarget()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())//If we click the left mouse button
        {
            //Makes a raycast from the mouse position into the game world
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition),Vector2.zero,Mathf.Infinity,512);

            if (hit.collider != null)//If we hit something
            {
                if (hit.collider.tag == "Enemy") //We check if we hit an enemy
                {
                    //If we hit an enemy the we set it as our target
                    player.MyTarget = hit.transform.GetChild(0);
                }
               
            }
            else
            {
                //Detargets the target
                player.MyTarget = null;
            }
        }
   
    }
}
